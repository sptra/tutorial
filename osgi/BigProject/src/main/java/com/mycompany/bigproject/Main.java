package com.mycompany.bigproject;

import com.mycompany.bigproject.moduleA.ModuleA;
import com.mycompany.bigproject.moduleA.impl.ModuleAImpl;

/**
 * Created by fani on 12/19/14.
 */
public class Main {

    public static void main(String[] args) {

        ModuleA m = new ModuleAImpl();  // <-- BAD

        m.methodPadaModuleA();

    }

}
