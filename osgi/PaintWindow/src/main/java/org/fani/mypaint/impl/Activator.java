package org.fani.mypaint.impl;

import org.fani.mypaint.PaintWindow;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

    static ServiceRegistration reg;
    static PaintWindowImpl window;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        if (window == null) {
            window = new PaintWindowImpl();
        }

        reg = bundleContext.registerService(PaintWindow.class.getName(),
                window, null);

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                window.setVisible(true);
            }
        });

//        System.out.println("Reg = " + reg);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        if (reg != null) {
            bundleContext.ungetService(reg.getReference());
        }
        window.dispose();
    }

}
