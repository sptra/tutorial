package org.fani.mypaint;

import org.fani.mypaint.impl.PaintWindowImpl;

public class App
{
    public static void main( String[] args )
            throws InterruptedException, java.lang.reflect.InvocationTargetException
    {
        javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                new PaintWindowImpl()
                        .setVisible(true);
            }
        });
    }
}
