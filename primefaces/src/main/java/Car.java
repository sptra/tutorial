/**
 * Created by sebastianvincent on 12/2/14.
 */
public class Car {
        private String model;
        private int year;
        private String manufacturer;
        private String color;
    public Car(String model, int year, String manufacturer, String color){
        this.model = model;
        this.year = year;
        this.manufacturer = manufacturer;
        this.color = color;
    }
    public String getManufacturer(){
        return manufacturer;
    }
    public String getModel(){
        return model;
    }
    public String getColor(){
        return color;
    }
    public int getYear(){
        return year;
    }
}
