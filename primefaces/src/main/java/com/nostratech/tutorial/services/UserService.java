package com.nostratech.tutorial.services;

import com.nostratech.tutorial.persistence.domain.User;
import com.nostratech.tutorial.persistence.repository.UserRepository;
import com.nostratech.tutorial.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by aguswinarno on 1/8/15.
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public UserService() {
    }

    public void save(UserVO userVO){

        User user = new User();

        user.setUserName(userVO.getUserName());
        user.setFirstName(userVO.getFirstName());
        user.setLastName(userVO.getLastName());
        user.setEmail(userVO.getEmail());
        user.setPassword(userVO.getPassword());
        user.setPhone(userVO.getPhone());
        user.setLocation(userVO.getLocation());
        user.setGender(userVO.getGender());
        user.setBirthday(userVO.getBirthday());
        user.setPhotoUrl(userVO.getPhotoUrl());

        user = userRepository.save(user);

    }
}
